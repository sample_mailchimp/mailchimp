# mailchimp

## Installation
Install package

```bash
pip install requirements.txt
```
## Usage

Copy .env.sample file to .env
```bash
cp .env.sample .env
```
Replace your apikey and url in .env file
```
APIKEY='<YOUR_API_KEY>'
URL = '<YOUR_URL>'
```


