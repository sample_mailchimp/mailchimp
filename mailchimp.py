import requests
from dotenv import load_dotenv
import os

load_dotenv()
username = 'test'
apikey = os.getenv('APIKEY')
url = os.getenv('URL')

post_params = {
    'members':[
        {
        'email_address': 'phatthanasorn.boat@gmail.com', 
        'status': 'subscribed',
        'merge_fields':{
                'FNAME':'test',
                'LNAME':'merge',
                'PHONE':'1111111111',
                'ADDRESS': {
                    "addr1" : "first line",
                    "addr2" : "second line",
                    "city" : "city",
                    "state": "state",
                    "zip": "zip code",
                    "country": "country"
                }
            }
        }
        ],
    'update_existing':True
}

r = requests.post(url, auth=(username, apikey), json=post_params)
# r = requests.delete(url, auth=(username, apikey))
r.raise_for_status()

results = r.json()
print(results)